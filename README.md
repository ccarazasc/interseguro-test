# interseguro-test

## Test de Interseguro

Para probar el funcionamiento acceder a la ruta realizando un Request POST: http://localhost:8080/matrix/rotate con un body de ejemplo:

```
{
    "values": [
        [1,2,3,4,5],
        [6,7,8,9,10],
        [11,12,13,14,15],
        [16,17,18,19,20],
        [21,22,23,24,25]
    ]
}
```
