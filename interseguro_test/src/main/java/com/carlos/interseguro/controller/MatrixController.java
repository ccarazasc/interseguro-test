package com.carlos.interseguro.controller;
import com.carlos.interseguro.exception.DataNotProvidedException;
import com.carlos.interseguro.model.Matrix;
import com.carlos.interseguro.service.MatrixService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/matrix")
public class MatrixController {

    private final Logger log = LoggerFactory.getLogger(MatrixController.class);

    @Autowired
    private MatrixService matrixService;

    @PostMapping("/rotate")
    public ResponseEntity<Matrix> rotate(@RequestBody Matrix matrixToRotate) throws Exception {
        log.debug("REST request to rotate Matrix: {}",matrixToRotate);
        if (matrixToRotate == null || matrixToRotate.getValues() == null || matrixToRotate.getValues().length == 0){
            throw new DataNotProvidedException("Data Not Provided");
        }
        Matrix rotatedMatrix = matrixService.rotate(matrixToRotate);

        return new ResponseEntity<Matrix>(rotatedMatrix, HttpStatus.OK);
    }
}
