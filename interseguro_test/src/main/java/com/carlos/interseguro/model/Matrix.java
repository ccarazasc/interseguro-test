package com.carlos.interseguro.model;

public class Matrix {

    private Integer[][] values;

    public Matrix(){};

    public Matrix(Integer n){
        this.values = new Integer[n][n];
    }

    public Integer[][] getValues() {
        return values;
    }

    public void setValues(Integer[][] values) {
        this.values = values;
    }

    public Integer getSize(){
        return this.values.length;
    }
}
