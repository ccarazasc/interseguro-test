package com.carlos.interseguro.service.impl;

import com.carlos.interseguro.model.Matrix;
import com.carlos.interseguro.service.MatrixService;
import org.springframework.stereotype.Service;

@Service
public class MatrixServiceImpl implements MatrixService {

    @Override
    public Matrix rotate(Matrix matrixToRotate) {

        int matrixSize = matrixToRotate.getSize();
        Matrix rotatedMatrix = new Matrix(matrixSize);
        Integer[][] valuesToRotate = matrixToRotate.getValues();

        for (int f = 0; f < (matrixSize/2); f++){
            for (int c = f; c < matrixSize - 1 - f; c++){
                int temp = valuesToRotate[f][c];
                valuesToRotate[f][c] = valuesToRotate[c][matrixSize-1-f];
                valuesToRotate[c][matrixSize-1-f] = valuesToRotate[matrixSize-1-f][matrixSize-1-c];
                valuesToRotate[matrixSize-1-f][matrixSize-1-c]=valuesToRotate[matrixSize-1-c][f];
                valuesToRotate[matrixSize-1-c][f] = temp;
            }
        }

        rotatedMatrix.setValues(valuesToRotate);
        return rotatedMatrix;
    }
}
