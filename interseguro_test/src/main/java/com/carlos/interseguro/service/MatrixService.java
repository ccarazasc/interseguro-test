package com.carlos.interseguro.service;

import com.carlos.interseguro.model.Matrix;
import org.springframework.stereotype.Service;

@Service
public interface MatrixService {

    Matrix rotate(Matrix matrixToRotate);
}
