package com.carlos.interseguro.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class DataNotProvidedException extends RuntimeException{
    public DataNotProvidedException(String message){
        super(message);
    }
}
